"""
Unsorted helper functions. Includes error computations and twiddle factor C
header generation.
"""

import os
from pathlib import Path

import numpy as np

import config
import fu

def step(num):
	"""Simple step function.

	For values < 0 returns 0, otherwise keeps the same value.

	Parameters
	----------
	num : any real number
		Input.

	Returns
	-------
	type is the same as input
	"""

	if (num < 0):
		return 0
	else:
		return num

def bit_pair_reverse(num, nbits):
	"""Split an input number into bit pairs and reverse their order.

	Start splitting from LSB. If there is an odd number of bits, the last
	(MSB) "bit pair" is only one bit.

	Parameters
	----------
	num : int
		Input number.
	nbits : numpy.int8 (max. 16)
		Number of bits to consider when reversing.

	Returns
	-------
	result : int
		Bit-pair reversed input.

	Examples
	--------
	>>> bit_pair_reverse(1,4)
	4  # 00|01 --> 01|00

	>>> bit_pair_reverse(1,5)
	8  # 0|00|01 --> 01|00|0

	>>> bit_pair_reverse(5,7)
	40  # 0|00|01|01 --> 01|01|00|0
	"""

	# nbits have to be signed integer; otherwise -nbits will be nonsense
	if nbits is not np.int8:
		nbits = np.int8(nbits)
	# Convert num to bitarray
	# > ... big endian, u2 ... unsigned 2 bytes (see numpy.ndarray doc)
	bits = np.unpackbits(np.array([num], dtype='>u2').view(np.uint8))
	bits = bits[-nbits:]
	# Split to bit pairs
	pairs = [bits[step(i-2):i] for i in range(len(bits), 0, -2)]
	# From bits back to numbers (pairs are already in a reversed order)
	try:
		pairs = np.concatenate(pairs)
	except:
		pairs = [0]
	pairs = np.lib.pad(pairs, (16-nbits, 0), 'constant',
	                   constant_values = (0, 0))
	result = (np.packbits(pairs)[0] << 8) + np.packbits(pairs)[1]
	return result

def mse(a, a_ref):
	"""Mean square error (MSE) of two complex vectors.

	Parameters
	----------
	a : array of complex
		Input vector.
	a_ref : array of complex
		Reference vector.

	Returns
	-------
	numpy.float64
		Mean squared error.
	"""

	# Array type check
	if not isinstance(a, np.ndarray):
		a = np.array(a)
	if not isinstance(a_ref, np.ndarray):
		a_ref = np.array(a_ref)
	# MSE
	return np.divide(np.sum(np.power(np.abs(a-a_ref),2)), len(a_ref))

def rel_err(a, a_ref):
	"""Relative error between two complex vectors.

	Relative error is a sum of distances between corresponding complex numbers
	a[i] and a_ref[i] divided by the biggest absolute value in a.

	Parameters
	----------
	a : array of complex
		Input vector.
	a_ref : array of complex
		Reference vector.

	Returns
	-------
	rel_err : numpy.float64 (size 2)
		Relative error
	"""

	# Array type check
	if not isinstance(a, np.ndarray):
		a = np.array(a)
	if not isinstance(a_ref, np.ndarray):
		a_ref = np.array(a_ref)
	# Errors
	abs_err = np.sum(np.abs(a-a_ref))
	rel_err = np.divide(abs_err, np.max(np.abs(a)))
	return rel_err

def ensure_dirs(path, path_is_file=False):
	"""Create all directories to 'path' if they don't exist.

	Returns True if directories were created, otherwise False.
	"""

	if not isinstance(path, Path):
		path = Path(path)

	if path_is_file:
		path = path.parent

	if not path.exists():
		os.makedirs(path)
		return True
	else:
		return False

def gen_tf_lutfile(path, Nexp=14):
	"""Generate C header file with LUT for FFT size 16384 (2**14).

	LUT (lookup table) file contains two constant arrays, each with 2049 (N/8+1)
	values. One contains real parts, the other imaginary parts of the twiddle
	factors.

	Parameters
	----------
	path : str
		Directory where to generate the file.
	Nexp : int
		FFT size as a power of two. Defaults to 14.

	Returns
	-------
	tf_fp : array of int16 (size (2049, 2))
	"""

	# Import is here because sys.path is not set yet when utils is loaded
	import fpconv

	if not isinstance(path, Path):
		path = Path(str(path))

	N = 2**Nexp
	tf_dec = fu.tf_lookup(Nexp)                   # Twiddle factors decimal
	tf_fp  = np.zeros([len(tf_dec),2], np.int16)  # Twiddle factors fixed point

	for i in range(len(tf_dec)):
		num = tf_dec[i]
		tf_fp[i,0] = fpconv.dec2fixed(np.real(num))
		tf_fp[i,1] = fpconv.dec2fixed(np.imag(num))

	# Printing a header file...
	with open(path.joinpath('tf_lut_16384.h'), 'w') as lutfile:
		lutfile.write("/* Twiddle factors for N = 16384 (2^14). Only N/8+1 values are stored\n")
		lutfile.write(" * (sector B0). Values are signed integers using a fixed point format (Q15).\n")
		lutfile.write(" * Each row is one number: first real, then imaginary part.\n")
		lutfile.write(" */\n\n")
		lutfile.write("#ifndef TF_LUT_16384_H\n")
		lutfile.write("#define TF_LUT_16384_H\n\n")
		lutfile.write("const short tf_lut_real[2049] = {\n")
		for i in range(len(tf_dec)):
			if (i == 0):
				lutfile.write("{0}".format(tf_fp[i,0]))
			else:
				lutfile.write(",\n{0}".format(tf_fp[i,0]))
		lutfile.write("\n};\n\n")
		lutfile.write("const short tf_lut_imag[2049] = {\n")
		for i in range(len(tf_dec)):
			if (i == 0):
				lutfile.write("{0}".format(tf_fp[i,1]))
			else:
				lutfile.write(',\n{0}'.format(tf_fp[i,1]))
		lutfile.write("\n};\n\n")
		lutfile.write("#endif /* TF_LUT_16384_H */\n")

	return tf_fp
