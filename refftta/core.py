#!/usr/bin/env python3
"""Second entry point.

This file can be run directly from an interactive Python shell. This allows to
keep input/output in the namespace inside the FFTComp class instance.

Usage:
	%run core.py [options]        IPython shell
	exec(open("core.py").read())  default Python shell

"""

import argparse
import os.path
import sys

import config
import inputs
import fftcomp
import ref
import utils

def main(fftComp=None):
	"""Main program. Highest level.

	Parameters
	----------
	fftComp : FFTComp
		FFTComp class instance which computes everything and keeps the results.
		If none is given, it creates one.

	Returns
	-------
	nothing so far

	Raises
	------
	ValueError
		If Nexp command line argument is not integer (except 'all' keyword) or
		out of the allowed range (specified in a config file).
	"""

	# Config
	try:
		config.init(root_search_path=os.path.abspath(__file__))
	except NameError:
		config.init(root_search_path=os.path.abspath("core.py"))
	config.cp.read(config.CFG_FILE)
	Nexp_min = int(config.cp['Limit_values']['nexp_min'])
	Nexp_max = int(config.cp['Limit_values']['nexp_max'])
	config.set_dir('INPUT', config.ROOT_DIR, config.cp['Paths']['input_dir'])
	config.set_dir('SHARE', config.ROOT_DIR, config.cp['Paths']['share_dir'])
	config.set_dir('TF_LUT', config.ROOT_DIR, config.cp['Paths']['tf_lut_dir'])
	config.set_dir('PY_REF', config.ROOT_DIR, config.cp['Paths']['py_ref_dir'])
	sys.path.append(str(config.DIRS['SHARE']))
	# fftComp instance
	if not isinstance(fftComp, fftcomp.FFTComp):
		fftComp = fftcomp.FFTComp()
	# Argument parsing
	ref_choices = ['out', 'tfg_tf', 'tfg_k', 'ag', 'inp', 'cadd', 'all']
	parser = argparse.ArgumentParser(
	           description="FFT computing toolset",
	           formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('Nexps', nargs='*', default='6',
	                    help="FFT length as a power of 2 (N = 2^Nexp).\
	                          Can be more values. If 'all', then computes\
	                          all values from {} to {}"
	                            .format(Nexp_min, Nexp_max))
	parser.add_argument('-i', type=str,
	                    choices=['cplx', 'cplx-pos', 'real', 'real-pos', 'csv'],
	                    default=config.cp['Input']['default_method'],
	                    help="Numbers in the input vector are random complex\
	                          [-1,1), complex positive [0,1), real [-1,1),\
	                          real positive [0,1) or read from a csv file.\
	                          In the csv case, specify the 'csv_group_dir'\
	                          parameter in the config file.")
	parser.add_argument('-n', '--normalize', action='store_true',
	                    help="Normalize the result so the highest absolute\
	                          value of either re/im part is 1.")
	parser.add_argument('-s', '--scale', action='store_true',
	                    help="Scale the output from complex adder as in C.")
	parser.add_argument('--gen-lut-header', action='store_true',
	                    help="Generate a C header with twiddle factors LUT.")
	parser.add_argument('-r', '--ref', nargs='*',
	                    choices=ref_choices, metavar='',
	                    help="Generate reference files. Allowed values are:\
	                          'out': FFT output;\
	                          'tfg_tf': Twiddle factors;\
	                          'tfg_k': Twiddle factor coefficients;\
	                          'ag': Operand addresses;\
	                          'inp': Input values;\
	                          'cadd': Complex adder testing values;\
	                          'all': All of them")
	args = parser.parse_args()

	if args.gen_lut_header:
		# Twiddle factor LUT C header file
		print("Generating a new twiddle factor LUT header file:")
		print(config.DIRS['TF_LUT'].joinpath('tf_lut_16384.h'))
		utils.gen_tf_lutfile(config.DIRS['TF_LUT'])
	else:
		# Setting Nexp list
		Nexps = nexps_list(args.Nexps, Nexp_min, Nexp_max)
		# Init values and memory allocations
		in_type_d = {'cplx': "random complex [-1,1).",
		             'cplx-pos': "random positive complex [0,1).",
		             'real': "random real [-1,1).",
		             'real-pos': "random positive real [0,1).",
		             'csv': "read from csv files:"}
		print("Input vector numbers are {}".format(in_type_d[args.i]))
		fftComp.setup(x_in=inputs.input_vector(Nexps, args.i),
		              Nexps=Nexps,
		              scale=args.scale,
		              normalize=args.normalize)

	if args.ref:
		# Reference files
		ref.create_refs(ref_outs(args.ref, ref_choices), fftComp)
	else:
		# Compute FFT with errors
		fftComp.compute_all()

def ref_outs(ref_arg, ref_choices):
	"""Which reference outputs to compute.
	"""

	if 'all' in ref_arg:
		ref_choices.remove('all')
		return ref_choices
	else:
		return ref_arg

def nexps_list(nexps_arg, Nexp_min, Nexp_max):
	"""List of FFT sizes to compute.
	"""

	if 'all' in nexps_arg:
		Nexps = list(range(Nexp_min, Nexp_max+1))
	else:
		try:
			Nexps = list(map(int, nexps_arg))
		except ValueError:
			raise TypeError("Only integers allowed as 'Nexp' (except 'all').")
		if not all(x in range(Nexp_min, Nexp_max+1) for x in Nexps):
			raise ValueError("'Nexp' must be within a range [{}, {}]"
			                   .format(Nexp_min, Nexp_max))
	Nexps.sort()
	return Nexps

if __name__ == "__main__":
	fftComp = fftcomp.FFTComp()
	main(fftComp)
