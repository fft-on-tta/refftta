"""
All project settings is stored here. This means mostly directory paths and
a config file parser.
"""

import os
import re
import io
from configparser import ConfigParser, ExtendedInterpolation
from pathlib import Path

DIRS = {}

def set_root_dir(path):
	"""Search through parents of a 'path' until 'config.txt' is found.

	Directory with 'config.txt' is then set to be a ROOT_DIR global variable.
	ROOT_DIR should be a path poining to the git repo root directory.

	Examples
	--------
	# start the search from the executed module path
	>>> set_root_dir(os.path.abspath(__file__))
	"""
	global ROOT_DIR
	p = Path(path)
	for parent in p.parents:
		if list(parent.glob('config.txt')):
			ROOT_DIR = parent.resolve()
			break

def set_cfgfile(cfg_filename='config.txt'):
	"""Set path to the config file into a CFG_FILE global variable.
	"""
	global CFG_FILE
	CFG_FILE = ROOT_DIR.joinpath(cfg_filename).resolve()

def set_configparser():
	"""Set a global config parser.
	"""
	global cp
	cp = ConfigCommentsParser(interpolation=ExtendedInterpolation())

def init(root_search_path, cfg_filename='config.txt'):
	"""Just calls the three previous functions to save space.
	"""

	set_root_dir(root_search_path)
	set_cfgfile(cfg_filename)
	set_configparser()

def set_dir(dir_name, root_dir, rel_path):
	"""Create a new directory entry to DIRS.

	'rel_path' is a relative path to the 'root_path'
	'root_dir' must be Path, rel_path can be string.
	"""
	DIRS[dir_name] = root_dir.joinpath(rel_path).resolve()

class ConfigCommentsParser(ConfigParser):
	"""Modified ConfigParser to store comments.

	Does not store inline comments. If a comment block contains empty lines it
	deletes them as well.

	Use as a standard ConfigParser. read() and write() methods were modified to
	store comments. To use
	"""
	def read(self, filenames, encoding=None, read_comments=True):
		super(ConfigCommentsParser, self).read(filenames, encoding)
		if read_comments:
			self.read_comments(filenames)

	def write(self, fp, space_around_delimiters=True, write_comments=True):
		if write_comments:
			with io.StringIO() as sio:
				super(ConfigCommentsParser, self).write(sio,
				                                        space_around_delimiters)
				sio.seek(0)
				self.insert_comments(sio, fp)
		else:
			super(ConfigCommentsParser, self).write(sio,
			                                        space_around_delimiters)

	def read_comments(self, cfgfiles):
		"""Read comments and save them together with their context.

		'cfgfiles' can be string, pathlike os a Path object (supported by open)
		as well as an iterable of these.
		"""
		if isinstance(cfgfiles, (str, os.PathLike)):
			cfgfiles = [cfgfiles]
		self.comments = {}
		for cfgfile in cfgfiles:
			try:
				file_key = str(cfgfile)
				with open(cfgfile, 'r') as rf:
					comment_block = []
					for line in rf:
						if line.startswith(self._comment_prefixes):
							comment_block.append(line)
						elif comment_block:
							if line.strip():
								dels = "|".join(self._delimiters)
								param = re.split(dels, line)[0].strip().lower()
								self.comments[param] = comment_block
								comment_block = []
					if comment_block:    # If last block is comments
						self.comments['*lastline'] = comment_block
			except OSError:
				continue

	def insert_comments(self, src_fp, dst_fp=None):
		"""Read 'in_fp' stream and insert comments according to 'self.comments'.
		Output is written to 'out_fp' stream.

		'in_fp' and 'out_fp' can be the same stream (i.e. a file opened in 'r+'
		mode). Also if 'out_fp' is not specified, it defaults to 'in_fp'.
		"""
		lines = src_fp.readlines()
		if not dst_fp:
			dst_fp = src_fp
		dst_fp.seek(0)
		for line in lines:
			try:
				for key in self.comments.keys():
					if line.startswith(key):
						dst_fp.writelines(self.comments[key])
						break
			except AttributeError:
				pass
			dst_fp.writelines(line)
		try:
			dst_fp.writelines(self.comments['*lastline'])
		except KeyError:
			pass
		except AttributeError:
			pass
		dst_fp.truncate()
