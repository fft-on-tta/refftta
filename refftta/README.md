# refftta/refftta #

refftta source files and input csv files.

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/refftta/refftta
├── input        // Folder with input csv files
├── config.py    // Program settings and config file parser
├── core.py      // Main program
├── fftcomp.py   // FFTComp class where all values lie
├── fu.py        // Fuctional units description (these really compute the FFT)
├── __init__.py  // Empty (indicates refftta is a Python module)
├── inputs.py    // FFT input values handling
├── __main__.py  // Allows running from commant line
├── ref.py       // Reference results generation
└── utils.py     // Misc assortment of functionsy
~~~~
