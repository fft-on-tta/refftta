"""
Input data loading/generation for computing FFT.
"""

import numpy as np

import config

def input_vector(Nexps, in_type, prints=True):
	"""Generate an input vector base on 'in_type'.

	Parameters
	----------
	Nexps : list of int
		FFT lengths as powers of 2 (N = 2**Nexp)
	in_type : str
		Select how to generate the input vector. Allowed values:
		{'csv', 'cplx', 'cplx-pos', 'real', 'real-pos'}

	Returns
	-------
	x_in : dict of complex np.arrays
		Input vectors for FFTs.

	Raises
	------
	ValueError
		If a given 'in_type' is not allowed.
	"""

	x_in = {}
	for Nexp in Nexps:
		N = 2**Nexp
		# Random complex [-1;1)
		if in_type == 'cplx':
			x_in[Nexp] = np.array( (2*np.random.rand(N)-1) \
			                     + (2*np.random.rand(N)-1)*1j)
		# Random complex [0;1)
		elif in_type == 'cplx-pos':
			x_in[Nexp] = np.array( (np.random.rand(N)) \
			                     + (np.random.rand(N))*1j)
		# Random real [-1;1)
		elif in_type == 'real':
			x_in[Nexp] = np.array( (2*np.random.rand(N)-1) \
			                     + (0)*1j)
		# Random real [0;1)
		elif in_type == 'real-pos':
			x_in[Nexp] = np.array( (np.random.rand(N)) \
			                     + (0)*1j)
		# Read from a csv file group
		elif in_type == 'csv':
			csv_file = csv_filename(Nexp, config.cp['Input']['csv_group_dir'])
			if prints:
				print(str(csv_file), flush=True)
			in_array = read_csv_file(csv_file)
			if len(in_array) != N:
				raise ValueError("{} does not have {} complex values."
				                   .format(csv_file.name, N))
			else:
				x_in[Nexp] = in_array
		else:
			raise ValueError("Wrong input type!")
	return x_in

def csv_filename(Nexp, csv_group):
	"""Return a csv filename as a Path object based on 'Nexp' and 'csv_group'.
	"""

	group_dir = config.DIRS['INPUT'].joinpath(csv_group)
	return group_dir.joinpath("in{}.csv".format(Nexp))

def read_csv_file(csv_file, delimiter=','):
	"""Read values from a .csv file into a vector x.
	"""

	data = np.genfromtxt(csv_file, delimiter=',')
	x = np.zeros(len(data), complex)
	for i,row in enumerate(data):
		x[i] = row[0] + row[1]*1j
	return x


# Dummy example for adding a custom input method
def add_inp_method(name, inp, x=None):
	if callable(inp):
		y = np.array(inp(x))
	else:
		y = np.array(inp)
	return y
