# refftta/input/randcplx #

Inputs are random complex numbers with real/imaginary part in the interval
[-1, 1). On every row, the first value is the real part, second value imaginary
part.

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/refftta/refftta/input/randcplx
├── in10.csv
├── in11.csv
├── in12.csv
├── in13.csv
├── in14.csv
├── in3.csv
├── in4.csv
├── in5.csv
├── in6.csv
├── in7.csv
├── in8.csv
└── in9.csv
~~~~
