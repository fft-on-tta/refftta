"""
Description of functional units and some helper functions.
"""

import numpy as np

import utils

# FUs
def ag(lin_idx, stage, Nexp, base_addr=0):
	"""Calculate addresses of 4 operands.

	From lin_idx it iterates to lin_idx+3, each time computing the address.

	Parameters
	----------
	lin_idx : int
		Initial address from which start computing addresses.
	stage : int
		Current computation stage of the FFT.
	Nexp : int
		FFT length as a power of two (N = 2**Nexp).
	base_addr : int, optional
		Starting address. Defaults to 0.

	Returns
	-------
	addr : array of uint16 (size 4)
		Indexes of four operands from the input array.
	"""

	nbits = Nexp
	addr = np.zeros(4, np.uint16)
	for i in range(4):
		# > ... big endian, u2 ... unsigned 2 bytes (see numpy.ndarray doc)
		idx = np.unpackbits(np.array([lin_idx+i], dtype='>u2').view(np.uint8))
		idx = idx[-nbits:]

		lsbs = np.array(idx[-2:])    # two LSBs
		idx = np.array(idx[:-2])     # remove the two LSBs from idx
		a = np.array(idx[:stage*2])  # split idx to parts a (left)
		b = np.array(idx[stage*2:])  #   and b (right)

		idx = np.concatenate((a, lsbs, b)) # insert lsbs between a and b

		idx = np.lib.pad(idx, (16-nbits, 0), 'constant',
		                 constant_values = (0, 0))
		addr[i] = (np.packbits(idx)[0] << 8) + np.packbits(idx)[1] + base_addr
	return addr

def tf_k(lin_idx, stage, Nexp, rdx2_flag, scale_k=True):
	"""Generate 4 twiddle factor indexes (k)

	This function generates 4 indexes (k) of 4 twiddle factors for 4 inputs of
	either one radix-4 or two radix-2 butterflies.

	Twiddle factor: (W_N)^k = e^(-j*k*2*pi/N)

	Parameters
	----------
	lin_idx : int
		Initial address from which start computing k.
	stage : int
		Current computation stage of the FFT.
	Nexp : int
		FFT length as a power of two (N = 2**Nexp).
	rdx2_flag : boolean
		Determines whether to compute radix-2 or radix-4 indexes.
	scale_k : boolean (default: True)
		Whether to scale the k. Unscaled k is for debugging only.

	Returns
	-------
	k : array of uint16 (size 4)
		Four indexes (k) of twiddle factors.
	"""

	if rdx2_flag == True:
		k = np.array([0, 1, 0, 1])
	else:
		k = np.array([0, 1, 2, 3])
	weight = np.uint16(0)
	total_stages = np.sum(list(stages_needed(Nexp).values()))
	# Working code for radix-4:
	idx = lin_idx >> (Nexp - 2*stage)
	Nexp_sc = 2*stage
	weight = utils.bit_pair_reverse(idx, Nexp_sc)
	k = np.multiply(k, weight)
	# Adjust to radix-2
	if rdx2_flag == True:
		k[3] = k[1] + 4**(total_stages-2)
	# Scale k
	sc = np.uint16(4**(7-stage-1))
	if (rdx2_flag == True):
		sc *= 2
	k_sc  = np.multiply(k, sc)
	if scale_k:
		return np.uint16(k_sc)
	else:
		return np.uint16(k)

def tfg(K, tf_lut, Nexp):
	"""Generate 4 twiddle factors from four indexes (K).

	It calculates the correct tf from the first 8th of all posible tf-s.

	Parameters
	----------
	K : array of int (size 4)
		Four tf indexes (k).
	tf_lut : array of complex (size(N/8+1,2))
		LUT with the first 8th of all possible tf-s.
	Nexp : int
		FFT size as a power of two (N = 2**Nexp).

	Returns
	-------
	W : array of complex (size 4)
		Four resulting twiddle factors.
	"""

	N = 2**Nexp
	eighth = np.uint16(N/8)
	W = np.zeros(4, complex)
	for i, k in enumerate(K):
		k = np.mod(k, N)
		if k <= eighth:                              # B0
			W[i] = tf_lut[k]
		elif k > 1*eighth and k <= 2*eighth :        # B1
			W[i] = -1j * np.conj(tf_lut[2*eighth-k])
		elif k > 2*eighth and k <= 3*eighth :        # B2
			W[i] = -1j * tf_lut[k-2*eighth]
		elif k > 3*eighth and k <= 4*eighth :        # B3
			W[i] = - np.conj(tf_lut[4*eighth-k])
		elif k > 4*eighth and k <= 5*eighth :        # B4
			W[i] = - tf_lut[k-4*eighth]
		elif k > 5*eighth and k <= 6*eighth :        # B5
			W[i] = 1j * np.conj(tf_lut[6*eighth-k])
		#elif k[i] > 6*eighth and k[i] <= 7*eighth :        # B6
		#	W[i] =  np.conj(tf_lut[k[i]-6*eighth])
		#elif k[i] > 7*eighth:                              # B7
		#	W[i] = np.conj(tf_lut[8*eighth-k[i]])
	return W

def cadd(P, rdx2_flag, scale=False):
	"""Complex adder (a butterfly operation (either one rdx-4 or two rdx-2)).

	Parameters
	----------
	P : array of complex (size 4))
		Input numbers.
	rdx2_flag : boolean
		Determines whether to perform one rdx-4 or two rdx-2 butterflies.
	scale : boolean
		Scale the result by 1/2 or 1/4 as in the C code.

	Returns
	-------
	Y : array of complex (size 4)
		Result.
	"""

	Y = np.zeros( 4, dtype=complex)
	if rdx2_flag is True:
		Y[0] = P[0] + P[1]
		Y[1] = P[0] - P[1]
		Y[2] = P[2] + P[3]
		Y[3] = P[2] - P[3]
		if scale is True:
			Y = np.divide(Y, 2)
	else:
		Y[0] = P[0] +    P[1] + P[2] +    P[3]
		Y[1] = P[0] - 1j*P[1] - P[2] + 1j*P[3]
		Y[2] = P[0] -    P[1] + P[2] -    P[3]
		Y[3] = P[0] + 1j*P[1] - P[2] - 1j*P[3]
		if scale is True:
			Y = np.divide(Y, 4)
	return Y

# Helper functions
def stages_needed(Nexp):
	"""Number of necessary radix-2 and -4 stages for FFT of size 2**Nexp.
	"""

	return { 'rdx4' : np.uint8(Nexp / 2),
	         'rdx2' : np.uint8(np.mod(Nexp, 2)) }

def tf_lookup(Nexp):
	"""Generate a lookup table (LUT) of twiddle factors.
	"""

	N = 2**Nexp
	M = np.uint16(N/8+1)
	twiddles = np.zeros(M, dtype=complex)
	for k in range(M):
		twiddles[k] = np.exp(-1j*2*np.pi*k/N)
	return twiddles
