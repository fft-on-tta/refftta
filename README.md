# refftta #

'refftta' stands for 'reference', 'FFT' and 'TTA'. It computes FFTs of given
lengths (2^k) in a way that resembles a schedule presented in [fft-in-tce][1].
It uses radix-4 algorithm. For 2^k where k is odd, the last stage is radix-2.
This approach is called mixed radix. For the computation, functional units
(FUs) are used. They are called in a more or less similar way as on the TTA
architecture.

The purpose of this program is to get familiar with the algorithm and to
produce reference results that can be tested when developing and simulating
the processor itself. It is possible to inspect all important values for all
computed FFT sizes in all stages.

[1]: https://gitlab.com/fft-on-tta/fft-in-tce

## Requirements ##
* Python >= 3.4 (it uses the new pathlib module)
* NumPy - a Python numerical computing package
* (recommended) iPython - enhanced Python shell
* probably Linux - I haven't tried other OSes but it might run there, too

## Usage ##

From command line execute from the repo root directory (where this README is):

~~~~
python refftta [options]
~~~~
If your default Python is Python 2 then use 'python3' instead of 'python'.

If you want to keep the fftComp object to inspect all the computed values,
run the refftta/core.py from a Python shell. For more about the fftComp object,
check its description in 'fftcomp.py'.

Running from an iPython shell (recommended):

~~~~
$ ls
config.txt  README.md  refftta
$ cd refftta
$ ipython
--- bunch of text, be sure it says Python at least 3.4 ---
In [1]: %run core.py [options]
--- computation output ---
In [2]: fftComp.y[6]
--- prints output values for FFT size 2^6 ---
~~~~

Running from a default Python shell (very clunky):

~~~~
$ ls
config.txt  README.md  refftta
$ cd refftta
$ python (or python3 if needed)
--- bunch of text, be sure it says Python  at least 3.4 ---
>>> import sys
>>> sys.argv = [options] (the first option is ignored; can be whatever)
>>> # example [options]: ['0', '5', '6', '-s'] (FFT sizes 2^5, 2^6, scaling)
>>> exec(open("core.py").read())
--- computation output ---
>>> fftComp.y[6]
--- prints output values for FFT size 2^6 ---
~~~~

List of available options:

~~~~
$ python refftta -h
usage: refftta [-h] [-i {cplx,cplx-pos,real,real-pos,csv}] [-n] [-s]
               [--gen-lut-header] [-r [[...]]]
               [Nexps [Nexps ...]]

FFT computing toolset

positional arguments:
  Nexps                 FFT length as a power of 2 (N = 2^Nexp). Can be more
                        values. If 'all', then computes all values from 3 to
                        14 (default: 6)

optional arguments:
  -h, --help            show this help message and exit
  -i {cplx,cplx-pos,real,real-pos,csv}
                        Numbers in the input vector are random complex [-1,1),
                        complex positive [0,1), real [-1,1), real positive
                        [0,1) or read from a csv file. In the csv case,
                        specify the 'csv_group_dir' parameter in the config
                        file. (default: csv)
  -n, --normalize       Normalize the result so the highest absolute value of
                        either re/im part is 1. (default: False)
  -s, --scale           Scale the output from complex adder as in C. (default:
                        False)
  --gen-lut-header      Generate a C header with twiddle factors LUT.
                        (default: False)
  -r [ [ ...]], --ref [ [ ...]]
                        Generate reference files. Allowed values are: 'out':
                        FFT output; 'tfg_tf': Twiddle factors; 'tfg_k':
                        Twiddle factor coefficients; 'ag': Operand addresses;
                        'inp': Input values; 'cadd': Complex adder testing
                        values; 'all': All of them (default: None)
~~~~

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/refftta
├── refftta     // Program sources
├── config.txt  // Specify paths, FFT size restrictions and input method here
└── README.md
~~~~
