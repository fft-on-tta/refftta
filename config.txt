[Paths]
; Shared files directory (necessary for outputting values)
; Set the path relative to this file ('..' means go one level up).
; Use UNIX path separators ('/').
share_dir = ../share
; Directory with csv input file directories
input_dir = refftta/input
; Twiddle factor LUT location
tf_lut_dir = ${share_dir}/tf_lut
; Python reference values folder
py_ref_dir = ${share_dir}/py_ref

[Limit_values]
nexp_max = 14
nexp_min = 3

[Input]
; Can be {csv, cplx, cplx-pos, real, real-pos}
default_method = csv
; Relevant only with default_method = csv
csv_group_dir = randcplx
